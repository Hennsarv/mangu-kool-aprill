﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mängukool
{
    class Program
    {
        static void Main(string[] args)
        {
            // kõiksuguste andmete sisselugemine failidest
            // kuidas sa alguses vajalikud andmed omale siia tekitad

            // test
            Console.WriteLine("\nKõik:\n");
            Inimene.Inimesed.Values.ToList().ForEach(x => Console.WriteLine(x));

            Console.WriteLine("\nÕpilased: \n");
            Inimene.Inimesed.Values.Where(x => x.Klass != "")
                .ToList()
                .ForEach(x => Console.WriteLine(x));

            Console.WriteLine("\nvanemad ja lapsed:\n");
            foreach(var v in Inimene.Inimesed.Values.Where(x => x.KasLapseVanem))
            {
                Console.WriteLine(v);
                Console.WriteLine("\ttema lapsed:");
                foreach (var laps in v.Lapsed3) Console.WriteLine($"\t{laps}");
            }


            // siit algab "sisselogimise" tsükkel
            string vastus;
            bool jätkame = true;
            do
            {
                Console.Write("Kes sa oled: ");
                vastus = Console.ReadLine();
                if (vastus != "") // siit kui sisestatid misngi isikukood
                {
                    Inimene kes = Inimene.ByCode(vastus);
                    if (kes != null)
                    {
                        // siia tegutsemine (kui sellise isikukoodiga inimene on olemas)
                        string küsimus = "Vali tegevus - (x) Lõpetame, (n) NImekiri, (h) Hinded: ";

                        // sõltuvalt sellest, kas õpilane, või õpetaja, või lapsevanem - on see "menüüvalik" erinev
                        Console.Write(küsimus);
                        // mis tähtedest koosneks 'menüü'
                        // kuidas see sõtuvalt asjaosalisest kokku panna

                        switch (Console.ReadLine().Substring(0,1).ToLower())
                        {
                            case "x" :
                                // mida teen siis kui see valik
                                return;
                               
                            case "n":

                                break;
                        }

                    }
                    else
                    {
                        Console.WriteLine("ei ei ei sind tunne ma\nmine proovi jälle sa");
                    }
                }
            } while (vastus != "" && jätkame);
        }

        static void ÕpilaseTegevused()
        {

        }
    }

    class MyClass
    {

    }
}
