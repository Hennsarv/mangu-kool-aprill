﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mängukool
{
    public class Inimene
    {
        public readonly String Isikukood;
        public static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();

        public string _Klass = ""; // klass kus õpib
        public string Klass { get => _Klass; set=> _Klass = value; }
        public string Aine = ""; // aine, mida õpetab
        public string Juhatab = ""; // klass, mida juhatab
        private string _Nimi = "";
     
        public List<Inimene> Lapsed = new List<Inimene>();

        public List<string> LasteKoodid = new List<string>();

        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = value.Length < 1 ? "" :  
                String.Join(" ",
                    value.Split(' ')
                    .Select(x => x.Substring(0, 1).ToUpper() + 
                                 x.Substring(1).ToLower())
                    );
        }

        // see (property) eeldab, et laste isikoodid on dictionaris olemas
        public IEnumerable<Inimene> Lapsed2 => 
            LasteKoodid
            .Select(x => Inimesed[x]);

        // see (property) lubab, et vanemal on isikukoode (lapsi), keda meie nimekirjas pole
        public IEnumerable<Inimene> Lapsed3 =>
            LasteKoodid
            .Where(x => Inimesed.ContainsKey(x))
            .Select(x => Inimesed[x]);

        // kaks kas-propertit - üks pikalt teine lühidalt
        public bool KasÕpilane => _Klass != "";
        public bool KasÕpetaja
        {
            get
            {
                return Aine != "";
            }
        }

        public bool KasLapseVanem => Lapsed.Count > 0;

        // static construktor
        // tema ülesanne on täita staatilised väljad
        // vajadusel teha majapidamise eeltööd
        static Inimene()
        {
            LoeAlgAndmed();
        }


        // objecti konstruktor
        // algväärtustada väljad
        // teha muid majapidamistöid - võtta object arvele
        protected Inimene(string isikukood)
        {
            Isikukood = isikukood;
            Inimesed.Add(isikukood, this);
        }

        // teha inimene isikukoodi alusel
        public static Inimene Create(string isikukood)
        {
            if (Inimesed.ContainsKey(isikukood))
                return Inimesed[isikukood];
            else
                return new Inimene(isikukood);
        }

        // leida inimene isikukoodi alusel
        public static  Inimene ByCode(string isikukood)
        {
            if (Inimesed.ContainsKey(isikukood))
                return Inimesed[isikukood];
            else return null;
        }

        public static void LoeAlgAndmed()
        {
            // TODO: siia paneme andmete sisselugemise osa
            string kaust = @"..\..\..\Data\";
            string õpilased = "õpilased.txt"; // IK, nimi, klass
            string õpetajad = "õpetajad.txt"; // IK, nimi, aine, klass
            string vanemad = "vanemad.txt";   // IK, nimi, laste-ik-d

            foreach (var rida in
            File.ReadAllLines(kaust + õpilased)
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray()))
            {
                Inimene i = Create(rida[0]);
                i.Nimi = rida[1];
                i.Klass = rida[2];
            }
            foreach (var rida in
            File.ReadAllLines(kaust + õpetajad)
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray()))
            {
                Inimene i = Create(rida[0]);
                i.Nimi = rida[1];
                i.Aine = rida[2];
                i.Juhatab = rida.Length < 4 ? "" : rida[3];
            }

            foreach (var rida in
            File.ReadAllLines(kaust + vanemad)
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray()))
            {
                Inimene i = Create(rida[0]);
                i.Nimi = rida[1];
                i.Lapsed = rida.Skip(2).Select(x => Inimene.ByCode(x)).ToList();
                i.LasteKoodid = rida.Skip(2).ToList();
            }


        }

        // see on vastik rida, seda ei saa tükeldada
        public override string ToString() => $"{(Klass != "" ? (Klass + " klassi õpilane ") : Aine != "" ? (Aine + " õpetaja " + (Juhatab == "" ? "" : $"ja {Juhatab} klassi juhataja ")) : "") }{Nimi} ({Isikukood}) {(Lapsed.Count > 0 ? $" {Lapsed.Count} lapse vanem" : "")}";
        
    }
}
