﻿namespace MängukooliAken
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ÕpilasedNupp = new System.Windows.Forms.Button();
            this.ÕpetajadNupp = new System.Windows.Forms.Button();
            this.KõikNupp = new System.Windows.Forms.Button();
            this.Tabelike = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.kesBox = new System.Windows.Forms.TextBox();
            this.Logimine_Nupp = new System.Windows.Forms.Button();
            this.Nimi = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Tabelike)).BeginInit();
            this.SuspendLayout();
            // 
            // ÕpilasedNupp
            // 
            this.ÕpilasedNupp.Location = new System.Drawing.Point(268, 87);
            this.ÕpilasedNupp.Name = "ÕpilasedNupp";
            this.ÕpilasedNupp.Size = new System.Drawing.Size(75, 23);
            this.ÕpilasedNupp.TabIndex = 0;
            this.ÕpilasedNupp.Text = "Õpilased";
            this.ÕpilasedNupp.UseVisualStyleBackColor = true;
            this.ÕpilasedNupp.Visible = false;
            this.ÕpilasedNupp.Click += new System.EventHandler(this.ÕpilasedNupp_Click);
            // 
            // ÕpetajadNupp
            // 
            this.ÕpetajadNupp.Location = new System.Drawing.Point(268, 87);
            this.ÕpetajadNupp.Name = "ÕpetajadNupp";
            this.ÕpetajadNupp.Size = new System.Drawing.Size(75, 23);
            this.ÕpetajadNupp.TabIndex = 1;
            this.ÕpetajadNupp.Text = "Õpetajad";
            this.ÕpetajadNupp.UseVisualStyleBackColor = true;
            this.ÕpetajadNupp.Visible = false;
            this.ÕpetajadNupp.Click += new System.EventHandler(this.ÕpetajadNupp_Click);
            // 
            // KõikNupp
            // 
            this.KõikNupp.Location = new System.Drawing.Point(420, 87);
            this.KõikNupp.Name = "KõikNupp";
            this.KõikNupp.Size = new System.Drawing.Size(75, 23);
            this.KõikNupp.TabIndex = 2;
            this.KõikNupp.Text = "Kõik";
            this.KõikNupp.UseVisualStyleBackColor = true;
            this.KõikNupp.Visible = false;
            this.KõikNupp.Click += new System.EventHandler(this.KõikNupp_Click);
            // 
            // Tabelike
            // 
            this.Tabelike.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Tabelike.Location = new System.Drawing.Point(121, 157);
            this.Tabelike.Name = "Tabelike";
            this.Tabelike.Size = new System.Drawing.Size(584, 150);
            this.Tabelike.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(121, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kes sa oled:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // kesBox
            // 
            this.kesBox.Location = new System.Drawing.Point(224, 27);
            this.kesBox.Name = "kesBox";
            this.kesBox.Size = new System.Drawing.Size(100, 20);
            this.kesBox.TabIndex = 5;
            // 
            // Logimine_Nupp
            // 
            this.Logimine_Nupp.Location = new System.Drawing.Point(362, 24);
            this.Logimine_Nupp.Name = "Logimine_Nupp";
            this.Logimine_Nupp.Size = new System.Drawing.Size(75, 23);
            this.Logimine_Nupp.TabIndex = 6;
            this.Logimine_Nupp.Text = "logi sisse";
            this.Logimine_Nupp.UseVisualStyleBackColor = true;
            this.Logimine_Nupp.Click += new System.EventHandler(this.Logimine_Nupp_Click);
            // 
            // Nimi
            // 
            this.Nimi.AutoSize = true;
            this.Nimi.Location = new System.Drawing.Point(121, 60);
            this.Nimi.Name = "Nimi";
            this.Nimi.Size = new System.Drawing.Size(0, 13);
            this.Nimi.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Nimi);
            this.Controls.Add(this.Logimine_Nupp);
            this.Controls.Add(this.kesBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Tabelike);
            this.Controls.Add(this.KõikNupp);
            this.Controls.Add(this.ÕpetajadNupp);
            this.Controls.Add(this.ÕpilasedNupp);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Tabelike)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ÕpilasedNupp;
        private System.Windows.Forms.Button ÕpetajadNupp;
        private System.Windows.Forms.Button KõikNupp;
        private System.Windows.Forms.DataGridView Tabelike;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kesBox;
        private System.Windows.Forms.Button Logimine_Nupp;
        private System.Windows.Forms.Label Nimi;
    }
}

