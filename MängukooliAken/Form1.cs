﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mängukool;

namespace MängukooliAken
{
    public partial class Form1 : Form
    {
        bool kasSees = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void ÕpilasedNupp_Click(object sender, EventArgs e)
        {
            Tabelike.DataSource = Inimene.Inimesed.Values
                .Where(x => x.KasÕpilane)
                .Select(x => new { x.Klass, x.Nimi, x.Isikukood})
                .ToList();

        }

        private void ÕpetajadNupp_Click(object sender, EventArgs e)
        {
            Tabelike.DataSource = Inimene.Inimesed.Values
             .Where(x => x.KasÕpetaja)
             .Select(x => new { x.Nimi, x.Isikukood, x.Aine, x.Juhatab })
             .ToList();
            

        }
        
        private void KõikNupp_Click(object sender, EventArgs e)
        {
            Tabelike.DataSource = Inimene.Inimesed.Values
              .Select(x => new { IK = x.Isikukood, x.Nimi })
              .ToList();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Logimine_Nupp_Click(object sender, EventArgs e)
        {
            if (kasSees)
            {
                kasSees = false;
                this.Logimine_Nupp.Text = "logi sisse";
                this.Nimi.Text = "";
                this.ÕpilasedNupp.Visible = false;
                this.ÕpetajadNupp.Visible = false;
                this.KõikNupp.Visible = false;
                Tabelike.DataSource = null;
                this.kesBox.Text = "";
            }
            else
            {
                string kesIsikukood = this.kesBox.Text;
                Inimene kes = Inimene.ByCode(kesIsikukood);
                if (kes != null)
                {
                    this.Nimi.Text = kes.ToString();
                    if (kes.KasÕpilane) this.ÕpilasedNupp.Visible = true;
                    if (kes.KasÕpetaja) this.ÕpetajadNupp.Visible = true;
                    this.KõikNupp.Visible = true;
                    this.Logimine_Nupp.Text = "logi välja";
                    this.kasSees = true;
                }
                else this.Nimi.Text = "võõras ära tule";
            }
        }
    }
}
